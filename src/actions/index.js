import * as types from "./ActionTypes";

// khai bao action
// su dung de goi 1 action trong component
// moi action chi co 1 type duy nhat
export const requestBlogStart = () => {
  return {
    type: types.BLOG_START
  }
};

export const requestBlogErrorStart = () => {
  return {
    type: types.BLOG_ERROR_START
  }
};
