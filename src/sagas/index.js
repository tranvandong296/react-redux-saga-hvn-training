import { put, takeLatest, all } from 'redux-saga/effects';

import * as types from "../actions/ActionTypes";

function * getBlogData() {
  // data moi se update vao Redux thong qua Saga
  // co the thay doi mock data ben duoi bang request den API voi async/await
  // vd: data = await axios.get("http://link-den-api.com");
  const data = [
    {
      id: 1,
      title: "Post 1",
      description: "Description post 1"
    },
    {
      id: 2,
      title: "Post 2",
      description: "Description post 2"
    },
    {
      id: 3,
      title: "Post 3",
      description: "Description post 3"
    },
    {
      id: 4,
      title: "Post 4",
      description: "Description post 4"
    },
    {
      id: 5,
      title: "Post 5",
      description: "Description post 5"
    },
  ];

  if (data.length > 0) {
    // truyen data den reducer voi type BLOG_SUCCESS
    yield put({type: types.BLOG_SUCCESS, payload: data});
  } else {
    // truyen data den reducer voi type BLOG_ERROR
    yield put({type: types.BLOG_ERROR});
  }
}

function * getBlogDataError() {
  // gia su data request loi
  // const data = [];
  // truyen message den reducer voi type BLOG_ERROR
  yield put({type: types.BLOG_ERROR, payload: "Data is empty!"});
}

// lang nghe action va goi Saga function
function * actionBlogWatcher() {
  yield takeLatest(types.BLOG_START, getBlogData);
}

function * actionBlogErrorWatcher() {
  yield takeLatest(types.BLOG_ERROR_START, getBlogDataError);
}

// chua tat ca cac action can phai lang nghe
export default function * root() {
  yield all([
    actionBlogWatcher(),
    actionBlogErrorWatcher()
  ])
}
