import * as types from "../actions/ActionTypes";

// khai bao default state object se luu trong redux store
const initialState = {
  data: [],
  message: null
};

// khai bao cac reducer se su dung
// reducer se luu data lay duoc tu saga vao redux store
// moi action type thuong se duoc luu vao 1 object rieng
const BlogReducer = (state = initialState, action) => {
  switch (action.type) {
    // update data nhan duoc tu saga vao initialState
    case types.BLOG_SUCCESS:
      return {
        ...state,
        data: action.payload,
        message: null
      };
    case types.BLOG_ERROR:
      return {
        ...state,
        data: [],
        message: action.payload
      };

    default:
      return state;
  }
};

export default BlogReducer;
