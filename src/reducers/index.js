import { combineReducers } from "redux";

import Blog from "./Blog";

// chua tat ca cac reducers
const rootReducer = combineReducers({
  Blog
});

export default rootReducer;
