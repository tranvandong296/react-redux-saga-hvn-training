import React, { Suspense } from 'react';
import {
  Switch,
  Route
} from "react-router-dom";

// import Homepage from "./screens/Homepage";
import AboutUs from "./screens/AboutUs";
import ContactUs from "./screens/ContactUs";
import Blog from "./screens/Blog";
import PostContent from "./screens/Blog/PostContent";
import NotFound from "./screens/NotFound";
import Header from "./partials/Header";
import Footer from "./partials/Footer";
import './App.css';

// su dung lazy load de tang performance khi tai trang
/* const AboutUs = lazy(() => import("./screens/AboutUs"));
const ContactUs = lazy(() => import("./screens/ContactUs"));
const Blog = lazy(() => import("./screens/Blog"));
const PostContent = lazy(() => import("./screens/Blog/PostContent"));
const NotFound = lazy(() => import("./screens/NotFound")); */

function App() {
  return (
    <div className="App">
      <Header />

      {/* Khai bao cac route se xu dung va main component cho cac route do */}
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={Blog} />
          <Route exact path="/about-us" component={AboutUs} />
          <Route exact path="/contact-us" component={ContactUs} />
          <Route exact path="/blog" component={Blog} />
          <Route path="/blog/:id" component={PostContent} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Suspense>

      <Footer />
    </div>
  );
}

export default App;
