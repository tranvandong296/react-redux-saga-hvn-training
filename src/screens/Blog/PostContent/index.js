import React from "react";

import NotFound from "../../NotFound";

const PostContent = ({ data, location }) => {
  const { pathname } = location;
  const postId = Number(pathname.replace("/blog/", ""));
  if (pathname && pathname !== "" && !Number.isNaN(postId)) {
    for (let i = 0; i < data.length; i++) {
      const post = data[i];
      if (post.id === postId) {
        return (
          <>
            <h1>{post.title}</h1>
            <p>{post.content}</p>
          </>
        );
      }
    }
  }

  return <NotFound />;
};

PostContent.defaultProps = {
  data: [
    {
      id: 1,
      title: "Post 1",
      content: "Content post 1"
    },
    {
      id: 2,
      title: "Post 2",
      content: "Content post 2"
    },
    {
      id: 3,
      title: "Post 3",
      content: "Content post 3"
    },
    {
      id: 4,
      title: "Post 4",
      content: "Content post 4"
    },
    {
      id: 5,
      title: "Post 5",
      content: "Content post 5"
    },
  ]
};

export default PostContent;
