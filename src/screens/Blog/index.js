import React, { useEffect }  from "react";
import { useDispatch, useSelector } from "react-redux";

import { requestBlogStart, requestBlogErrorStart } from "../../actions";
import { Button } from "reactstrap";
import Post from "./Post";
import "./Blog.scss";

const Blog = () => {
  // Lay state tu Redux store
  // useSelector su dung de tranh lay lai data da duoc load truoc do
  const { blog, message } = useSelector(state => {
    return {
      blog: state.Blog.data,
      message: state.Blog.message
    }
  });
  // dung dispatch de goi Redux action
  const dispatch = useDispatch();

  // Su dung useEffect de goi action moi khi load page.
  // Neu ko su dung Redux action, co the send request va nhan data tu API voi useEffect
  // sau do set data nhan duoc vao state cua component.
  useEffect(() => {
    // goi den Redux action de lay data tu mock data hoac API
    dispatch(requestBlogStart());
  }, [dispatch]);

  if (message !== null) {
    return <h4>{message}</h4>;
  }

  // su dung loop de tai su dung lai component con
  const listItems = blog.map((item, index) => {
    return <Post key={index} item={item} />
  });

  return (
    <>
      <Button color="danger" onClick={() => dispatch(requestBlogErrorStart())}>Request Error</Button>
      {/* hien thi Blog list tu cac component da duoc render o tren */}
      <ul className="list-items">{listItems}</ul>
    </>
  );
};

export default Blog;
